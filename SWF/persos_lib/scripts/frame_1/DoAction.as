function _setColor(mc, col)
{
   var _loc1_ = {r:col >> 16,g:col >> 8 & 255,b:col & 255};
   var _loc2_ = new Color(mc);
   var _loc3_ = {ra:100,ga:100,ba:100,aa:100,rb:_loc1_.r - 255,gb:_loc1_.g - 255,bb:_loc1_.b - 255,ab:0};
   _loc2_.setTransform(_loc3_);
}
