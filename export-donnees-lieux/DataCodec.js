function codecInit(key)
{
	let v3 = 0;
	let v4 = 0;
	let v1 = key + "";
	let v2 = new Array(256);
	while(v3 < 256)
	{
		v4 = v3++;
		v2[v4] = v4 & 127;
	}

	v3 = 0;
	v4 = v1.length;

	let v5 = 0;
	while(v5 < 256)
	{
		let v6 = v5++;
		v3 = (v3 + v2[v6] + v1.charCodeAt(v6 % v4)) & 127;
		let v7 = v2[v6];
		v2[v6] = v2[v3];
		v2[v3] = v7;
	}

	return v2;
}

function codecDeserialize(key, param1)
{
	let s = codecInit(key);
	let v3 = s;
	let v4 = '';
	let v5 = param1.length - 4;
	let v6 = v3[0];
	let v7 = v3[1];
	let v8 = 0;

	while(v8 < v5)
	{
		let v9 = v8++;
		let v10 = param1.charCodeAt(v9);
		let v11 = v10 ^ v3[v9 & 255];
		let v12 = v11 == 0 ? v10 : v11;
		v4 = v4 + String.fromCharCode(v12);
		if(v11 == 0)
		{
			v10 = 0;
		}
		v6 = (v6 + v10) % 65521;
		v7 = (v7 + v6) % 65521;
	}
	v8 = v6 ^ (v7 << 8);
	v5++;
	return v4;
}

function codecSerialize(key, param1)
{
	let ENCODE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
	let s = codecInit(key);
	let v8 = 0;
	let v9 = null;
	let v10 = 0;
	let v11 = 0;
	var v2 = s;
	var v3 = '';
	var v4 = v2[0];
	var v5 = v2[1];
	var v6 = 0;
	var v7 = param1.length;
	while(v6 < v7)
	{
		v8 = v6++;
		v9 = param1.charCodeAt(v8);
		v10 = v9 ^ v2[v8 & 255];
		v11 = v10 === 0 ? v9 : v10;
		v3 = v3 + String.fromCharCode(v11);
		v4 = (v4 + v10) % 65521;
		v5 = (v5 + v4) % 65521;
	}
	v6 = v4 ^ (v5 << 8);
	v7 = ENCODE.charCodeAt(v6 & 63);
	v3 = v3 + String.fromCharCode(v7);
	v7 = ENCODE.charCodeAt((v6 >> 6) & 63);
	v3 = v3 + String.fromCharCode(v7);
	v7 = ENCODE.charCodeAt((v6 >> 12) & 63);
	v3 = v3 + String.fromCharCode(v7);
	v7 = ENCODE.charCodeAt((v6 >> 18) & 63);
	v3 = v3 + String.fromCharCode(v7);
	return v3;
}

exports.codecDeserialize = codecDeserialize;