# Extracting location data
## What you will need
* node
* A browser with Flash installed (you don't need to actually be able to run it, just have Flash installed)
* Recommended: the Eternal-Twin browser.

## How much time you will need
About two minutes per zone.
## How?
First, go to a zone that hasn't been exported yet. You can use the Eternal-Twin browser to move from zone to zone. Start a game there. You don't need to actually *play*, just be on the play page. Allow your browser to run Flash. You should see the timebomb error message:

![enter image description here](https://i.imgur.com/wIOKBbH.png)


From there, use the Inspect tool near the SWF player:

![enter image description here](https://i.imgur.com/JH3ovxX.png)


In the developer tools window, copy the **entire** `flashvars` value. In the above screenshot, the value starts with "d=". Copy it all into the data.txt file in the repo.

The flashvars should look like this:

d=\<a lot of stuff>&sid=\<short data>&n=\<short data>&k=\<short data>&l=\<short data>&u=\<a URL>&ud=\<a URL>&sv=1. 

If it's the case: great! If not, contact Docteur on the Eternal-Twin Discord so we can work it out together.

Once this is done, run the following line:

`node extract.js`

This will create 4 JSON files in the /out folder. Now, go to the out folder, create a folder with the full name of the area, and copy all four files inside of it. Commit, work done. 
