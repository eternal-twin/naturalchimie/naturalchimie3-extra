


var fs = require('fs');
var path = require('path');

const transformFileToSerialized = () => (filename, id, folder) =>
{
	let locationData = {};
    let init = fs.readFileSync(`${folder}/${filename}.json`, 'utf8');
	let json_object = JSON.parse(init);
	let chain = [];
	for(let i = 0; i < json_object._chain.length; i++){
		chain[i] = json_object._chain[i][1][0]
	}
	locationData.chain = chain;
	locationData.objects_allowed = json_object._object;
	locationData.music = json_object._music;
	locationData.tuples = json_object._artefacts;
	locationData.name = folder;
	locationData.id = id;
	//console.log(locationData);
	return locationData;
}

const runTransformFileToSerialized = transformFileToSerialized();

var source = ".";

fs.readdir(source, function (err, files) {
  if (err) {
    console.error("Could not list the directory.", err);
    process.exit(1);
  }
  var i = 0;
  locationData = [];
  files.forEach(function (file, index) {
	console.log(i);
	
	if(file!=='extract.js' && file!=='location.json'){
		locationData[i] = runTransformFileToSerialized("data", i, file);
		i++;
	}
  });
  
  let jsonResult = JSON.stringify(locationData);
  fs.writeFileSync(`location.json`,jsonResult);
});