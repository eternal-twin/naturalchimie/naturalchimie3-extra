const rp = require('request-promise').defaults({ jar: true });
const tough = require('tough-cookie'); 
const $ = require('cheerio');
var fs = require('fs');


function scrape(start_id, max_id) {
let cookie = new tough.Cookie({
	key: "sid",
	value: "PUT_VALID_SESSION_ID",
	domain: "www.naturalchimie.com",
	maxAge: 31536000
});


var cookiejar = rp.jar();

const _include_headers = function(body, response, resolveWithFullResponse) {
    return {
        'data': body, 
        'finalUrl': response.request.uri.href // contains final URL
    };
};

cookiejar.setCookie(cookie.toString(), 'http://www.naturalchimie.com');

	var options_tab = [];
	for(let id = start_id; id <= max_id; id++){
		console.log("Prepping chemist " + id);
		var options = {
			uri: 'http://www.naturalchimie.com/user/' + id,
			jar: cookiejar,
			transform: _include_headers,
			headers: {
				'cookie': 'sid=PUT_VALID_SESSION_ID'
			}
		};
		
		options_tab.push(rp(options));
	}

	Promise.all(options_tab.map(p => p.catch(e=>e)))
		.then((results, error, html_value) => {
			for(let i = 0; i < results.length; i++){
				let html = results[i].data;
				console.log(results[i].finalUrl);
				if(results[i].finalUrl.split('/').length !== 5){
					continue;
				}
				var chemist = {}
				chemist.id = results[i].finalUrl.split('/')[4];
				chemist.username = $('.userName', html)[0].children[0].data;
				chemist.lvl = $('.lvl', html)[0].children[0].data;
				chemist.rank = $('.rank > span', html)[0].children[0].data;
				chemist.school = $('.school > span', html)[0].children[2].data.slice(1);
				if($('.xp > a', html)[0] != undefined){
					chemist.classroom = $('.xp > a', html)[0].children[0].data;
				}

				chemist.fioles = [];
				for (let i = 0; i < 5; i++) {
					let fiole = {};
					fiole.name = $('.alchLevel > div', html)[i].attribs.onmouseover.split(',')[1].slice(1,-1);
					fiole.xp = $('.alchLevel > div', html)[i].attribs.onmouseover.split(',')[2].slice(1,-1);
					chemist.fioles[i] = fiole;
				}
				chemist.reputs = []
				for (let i = 0; i < 5; i++) {
					let reput = {}
					reput.school = $('.rep_bar > .zeschool', html)[i].children[0].next.attribs.onmouseover.split(',')[1].slice(1,-1);
					reput.level = $('.rep_bar > .repName', html)[i].children[0].data;
					reput.percentage = $('.rep_bar > .rep_perc', html)[i].children[0].parent.attribs.onmouseover.split(',')[1].slice(1,-1);
					chemist.reputs[i] = reput;
				}
				chemist.face = $('.myChemist > script', html)[0].children[0].data.split('\n')[3].split(',')[1].slice(2, -4);
				chemist.artefacts = [];
				for (let j = 0; j < Object.keys($('.artefact', html)).length; j++){
					if($('.artefact', html)[j] == undefined ){
						break;
					}
					chemist.artefacts[j] = $('.artefact', html)[j].attribs.onmouseover.split(',')[1].slice(1,-1);
				}
				console.log('Writing id ' + results[i].finalUrl.split('/')[4]);
			fs.writeFileSync(`out/${results[i].finalUrl.split('/')[4]}.json`, JSON.stringify(chemist));
			}
		}).catch(err => console.log(err));  
}

exports.Scrape = (start_id, max_id) => (
    scrape(start_id, max_id)
);
